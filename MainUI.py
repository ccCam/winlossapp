# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(457, 367)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setObjectName("comboBox")
        self.verticalLayout.addWidget(self.comboBox)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.lblTotal = QtWidgets.QLabel(self.centralwidget)
        self.lblTotal.setStyleSheet("}")
        self.lblTotal.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTotal.setObjectName("lblTotal")
        self.gridLayout.addWidget(self.lblTotal, 4, 1, 1, 1)
        self.btnDailyWin = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyWin.setObjectName("btnDailyWin")
        self.gridLayout.addWidget(self.btnDailyWin, 1, 0, 1, 1)
        self.btnDailyLoss = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyLoss.setObjectName("btnDailyLoss")
        self.gridLayout.addWidget(self.btnDailyLoss, 1, 2, 1, 1)
        self.btnTotalWin = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalWin.setObjectName("btnTotalWin")
        self.gridLayout.addWidget(self.btnTotalWin, 4, 0, 1, 1)
        self.btnTotalLoss = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalLoss.setObjectName("btnTotalLoss")
        self.gridLayout.addWidget(self.btnTotalLoss, 4, 2, 1, 1)
        self.lblDaily = QtWidgets.QLabel(self.centralwidget)
        self.lblDaily.setStyleSheet("")
        self.lblDaily.setAlignment(QtCore.Qt.AlignCenter)
        self.lblDaily.setObjectName("lblDaily")
        self.gridLayout.addWidget(self.lblDaily, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnDailyReset = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyReset.setObjectName("btnDailyReset")
        self.horizontalLayout.addWidget(self.btnDailyReset)
        self.btnTotalReset = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalReset.setObjectName("btnTotalReset")
        self.horizontalLayout.addWidget(self.btnTotalReset)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.lblTotal.setText(_translate("MainWindow", "0 ALL TIME 0"))
        self.btnDailyWin.setText(_translate("MainWindow", "Win"))
        self.btnDailyLoss.setText(_translate("MainWindow", "Loss"))
        self.btnTotalWin.setText(_translate("MainWindow", "Win"))
        self.btnTotalLoss.setText(_translate("MainWindow", "Loss"))
        self.lblDaily.setText(_translate("MainWindow", "0 DAILY 0"))
        self.btnDailyReset.setText(_translate("MainWindow", "Reset Daily"))
        self.btnTotalReset.setText(_translate("MainWindow", "Reset All Time"))

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(457, 367)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setEditable(True)
        self.comboBox.setMaxVisibleItems(100)
        self.comboBox.setObjectName("comboBox")
        self.verticalLayout.addWidget(self.comboBox)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.lblTotal = QtWidgets.QLabel(self.centralwidget)
        self.lblTotal.setStyleSheet("}")
        self.lblTotal.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTotal.setObjectName("lblTotal")
        self.gridLayout.addWidget(self.lblTotal, 4, 1, 1, 1)
        self.btnDailyWin = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyWin.setObjectName("btnDailyWin")
        self.gridLayout.addWidget(self.btnDailyWin, 1, 0, 1, 1)
        self.btnDailyLoss = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyLoss.setObjectName("btnDailyLoss")
        self.gridLayout.addWidget(self.btnDailyLoss, 1, 2, 1, 1)
        self.btnTotalWin = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalWin.setObjectName("btnTotalWin")
        self.gridLayout.addWidget(self.btnTotalWin, 4, 0, 1, 1)
        self.btnTotalLoss = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalLoss.setObjectName("btnTotalLoss")
        self.gridLayout.addWidget(self.btnTotalLoss, 4, 2, 1, 1)
        self.lblDaily = QtWidgets.QLabel(self.centralwidget)
        self.lblDaily.setStyleSheet("")
        self.lblDaily.setAlignment(QtCore.Qt.AlignCenter)
        self.lblDaily.setObjectName("lblDaily")
        self.gridLayout.addWidget(self.lblDaily, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnDailyReset = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyReset.setObjectName("btnDailyReset")
        self.horizontalLayout.addWidget(self.btnDailyReset)
        self.btnTotalReset = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalReset.setObjectName("btnTotalReset")
        self.horizontalLayout.addWidget(self.btnTotalReset)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.lblTotal.setText(_translate("MainWindow", "0 ALL TIME 0"))
        self.btnDailyWin.setText(_translate("MainWindow", "Win"))
        self.btnDailyLoss.setText(_translate("MainWindow", "Loss"))
        self.btnTotalWin.setText(_translate("MainWindow", "Win"))
        self.btnTotalLoss.setText(_translate("MainWindow", "Loss"))
        self.lblDaily.setText(_translate("MainWindow", "0 DAILY 0"))
        self.btnDailyReset.setText(_translate("MainWindow", "Reset Daily"))
        self.btnTotalReset.setText(_translate("MainWindow", "Reset All Time"))

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(457, 367)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMaximumSize(QtCore.QSize(100, 50))
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setMaximumSize(QtCore.QSize(16777215, 25))
        self.comboBox.setEditable(True)
        self.comboBox.setMaxVisibleItems(100)
        self.comboBox.setObjectName("comboBox")
        self.horizontalLayout_2.addWidget(self.comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setObjectName("gridLayout")
        self.lblTotal = QtWidgets.QLabel(self.centralwidget)
        self.lblTotal.setStyleSheet("}")
        self.lblTotal.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTotal.setObjectName("lblTotal")
        self.gridLayout.addWidget(self.lblTotal, 4, 1, 1, 1)
        self.btnDailyWin = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyWin.setObjectName("btnDailyWin")
        self.gridLayout.addWidget(self.btnDailyWin, 1, 0, 1, 1)
        self.btnDailyLoss = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyLoss.setObjectName("btnDailyLoss")
        self.gridLayout.addWidget(self.btnDailyLoss, 1, 2, 1, 1)
        self.btnTotalWin = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalWin.setObjectName("btnTotalWin")
        self.gridLayout.addWidget(self.btnTotalWin, 4, 0, 1, 1)
        self.btnTotalLoss = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalLoss.setObjectName("btnTotalLoss")
        self.gridLayout.addWidget(self.btnTotalLoss, 4, 2, 1, 1)
        self.lblDaily = QtWidgets.QLabel(self.centralwidget)
        self.lblDaily.setStyleSheet("")
        self.lblDaily.setAlignment(QtCore.Qt.AlignCenter)
        self.lblDaily.setObjectName("lblDaily")
        self.gridLayout.addWidget(self.lblDaily, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnDailyReset = QtWidgets.QPushButton(self.centralwidget)
        self.btnDailyReset.setObjectName("btnDailyReset")
        self.horizontalLayout.addWidget(self.btnDailyReset)
        self.btnTotalReset = QtWidgets.QPushButton(self.centralwidget)
        self.btnTotalReset.setObjectName("btnTotalReset")
        self.horizontalLayout.addWidget(self.btnTotalReset)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Wins and Losses"))
        self.label.setText(_translate("MainWindow", "Current Game"))
        self.lblTotal.setText(_translate("MainWindow", "0 ALL TIME 0"))
        self.btnDailyWin.setText(_translate("MainWindow", "Win"))
        self.btnDailyLoss.setText(_translate("MainWindow", "Loss"))
        self.btnTotalWin.setText(_translate("MainWindow", "Win"))
        self.btnTotalLoss.setText(_translate("MainWindow", "Loss"))
        self.lblDaily.setText(_translate("MainWindow", "0 DAILY 0"))
        self.btnDailyReset.setText(_translate("MainWindow", "Reset Daily"))
        self.btnTotalReset.setText(_translate("MainWindow", "Reset Total"))

