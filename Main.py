from PyQt5 import QtCore, QtGui, QtWidgets
from MainUI import Ui_MainWindow
from os import listdir
from os.path import isfile, join
import os
import sys

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

	def __init__(self, parent=None):
			QtWidgets.QWidget.__init__(self, parent=parent)
			self.setupUi(self)
			
			self.btnDailyWin.clicked.connect(self.daily_win)
			self.btnDailyLoss.clicked.connect(self.daily_loss)
			self.btnDailyReset.clicked.connect(self.daily_reset)
			
			self.btnTotalWin.clicked.connect(self.total_win)
			self.btnTotalLoss.clicked.connect(self.total_loss)
			self.btnTotalReset.clicked.connect(self.total_reset)
			
			self.comboBox.activated.connect(self.combo_changed)
			
			
			self.populate_combobox()
			self.populate_labels()
			
			
	def daily_win(self):
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		
		
		file_text = fr.read()
		
		file_text = file_text.splitlines()
		
		
		file_text_split = file_text[0].split()
		
		newWins = 0
		newLosses = 0
		
		print(file_text_split)
		
		try:
			newWins = int(file_text_split[0]) + 1
			newLosses = int(file_text_split[2])
		except:
			print("Except")
			newWins = 1
			newLosses = 0
		
		fw = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "w")
		fw.write(str(newWins) + " DAILY " + str(newLosses) + "\n" + file_text[1])
		fw.close()
		
		fr.close()
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		file_text = fr.read().splitlines()
		
		self.lblDaily.setText(file_text[0])
		print(self.lblDaily.text())
		
	def daily_loss(self):
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		
		
		file_text = fr.read()
		
		file_text = file_text.splitlines()
		
		
		file_text_split = file_text[0].split()
		
		newWins = 0
		newLosses = 0
		
		print(file_text_split)
		
		try:
			newWins = int(file_text_split[0])
			newLosses = int(file_text_split[2]) + 1
		except:
			print("Except")
			newWins = 1
			newLosses = 0
		
		fw = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "w")
		fw.write(str(newWins) + " DAILY " + str(newLosses) + "\n" + file_text[1])
		fw.close()
		
		fr.close()
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		file_text = fr.read().splitlines()
		
		self.lblDaily.setText(file_text[0])
		print(self.lblDaily.text())
		
	def daily_reset(self):
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		file_text = fr.read()
		
		file_text = file_text.splitlines()
		fw = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "w")
		fw.write("0 DAILY 0" + "\n" + file_text[1])
		
		self.lblDaily.setText("0 DAILY 0")
		
		
	def total_win(self):
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		
		
		file_text = fr.read()
		
		file_text = file_text.splitlines()
		
		
		file_text_split = file_text[1].split()
		
		newWins = 0
		newLosses = 0
		
		print(file_text_split)
		
		try:
			newWins = int(file_text_split[0]) + 1
			newLosses = int(file_text_split[2])
		except:
			print("Except")
			newWins = 1
			newLosses = 0
		
		fw = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "w")
		fw.write(file_text[0] + "\n" + str(newWins) + " TOTAL " + str(newLosses))
		fw.close()
		
		fr.close()
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		file_text = fr.read().splitlines()
		
		self.lblTotal.setText(file_text[1])
		print(self.lblDaily.text())
		
	def total_loss(self):
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		
		
		file_text = fr.read()
		
		file_text = file_text.splitlines()
		
		file_text_split = file_text[1].split()
		
		newWins = 0
		newLosses = 0
		
		print(file_text_split)
		
		try:
			newWins = int(file_text_split[0])
			newLosses = int(file_text_split[2]) + 1
		except:
			print("Except")
			newWins = 1
			newLosses = 0
		
		fw = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "w")
		fw.write(file_text[0] + "\n" + str(newWins) + " TOTAL " + str(newLosses))
		fw.close()
		
		fr.close()
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		file_text = fr.read().splitlines()
		
		self.lblTotal.setText(file_text[1])
		print(self.lblDaily.text())
		
	def total_reset(self):
		fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
		file_text = fr.read()
		
		file_text = file_text.splitlines()
		fw = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "w")
		fw.write(file_text[1] + "\n" + "0 TOTAL 0")
		
		self.lblTotal.setText("0 TOTAL 0")


	def combo_changed(self, index):
		print(self.comboBox.itemText(index))
		print(self.comboBox.count())
		
		mypath = os.path.expanduser("~/.config/WinLossApp/")
		
		if not os.path.exists(mypath + self.comboBox.itemText(index) + ".txt"):
			f = open(mypath + self.comboBox.itemText(index) + ".txt", "w")
			f.write("0 DAILY 0\n0 TOTAL 0")
			f.close()
		
		self.populate_labels()



	def populate_combobox(self):
		
		print(self.comboBox.count())
		
		mypath = os.path.expanduser("~/.config/WinLossApp/")
		
		
		if not os.path.exists(mypath):
			os.makedirs(mypath)
			
		onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
		for item in onlyfiles:
			print(item)
			
			self.comboBox.addItem(str(item.split(".")[0]))
			
			
		print(self.comboBox.count())

	def populate_labels(self):
		try:
			fr = open(os.path.expanduser("~/.config/WinLossApp/" + self.comboBox.currentText() + ".txt"), "r")
			strings = []
			strings = fr.read().splitlines()
			print(strings)
			try:
				self.lblDaily.setText(strings[0])
			except:
				self.lblDaily.setText("0 DAILY 0")
			try:
				self.lblTotal.setText(strings[1])
			except:
				self.lblTotal.setText("0 TOTAL 0")
			
			fr.close()
		except:
			pass
			
		
		
		
		
		

#Launch the main section of the app
if __name__ == '__main__':
	app = QtWidgets.QApplication(sys.argv)
	window = MainWindow()
	window.show()
	sys.exit(app.exec_())
